<?php

namespace Mobiteasy\Generated\Model;

abstract class Record_Offer extends \Connect\Database\ActiveRecord\Record
{
	protected $_settings;

	protected $_urlStatus;

	public function __construct( $id = null )
	{
		parent::_initialize();
		if ( null !== $id )
		{
			parent::load( array( 'id' => $id ) );
		}
		parent::_postInitialize();
	}

	/**
	 * @return int INT(32) NULL
	 */
	public function getId()
	{
		return $this->_getValue('id');
	}

	/**
	 * @param int $value INT(32) NULL
	 */
	public function setId( $value )
	{
		$this->_setValue( 'id', $value );
	}

	/**
	 * @return int INT(32) NULL
	 */
	public function getOldId()
	{
		return $this->_getValue('old_id');
	}

	/**
	 * @param int $value INT(32) NULL
	 */
	public function setOldId( $value )
	{
		$this->_setValue( 'old_id', $value );
	}

	/**
	 * @return int INT(32)
	 */
	public function getPartnerId()
	{
		return $this->_getValue('partner_id');
	}

	/**
	 * @param int $value INT(32)
	 */
	public function setPartnerId( $value )
	{
		$this->_setValue( 'partner_id', $value );
	}

	/**
	 * @return int INT(32)
	 */
	public function getTrafficTypeId()
	{
		return $this->_getValue('traffic_type_id');
	}

	/**
	 * @param int $value INT(32)
	 */
	public function setTrafficTypeId( $value )
	{
		$this->_setValue( 'traffic_type_id', $value );
	}

	/**
	 * @return int INT(32)
	 */
	public function getManagerId()
	{
		return $this->_getValue('manager_id');
	}

	/**
	 * @param int $value INT(32)
	 */
	public function setManagerId( $value )
	{
		$this->_setValue( 'manager_id', $value );
	}

	/**
	 * @return string VARCHAR(255) NULL
	 */
	public function getName()
	{
		return $this->_getValue('name');
	}

	/**
	 * @param string $value VARCHAR(255) NULL
	 */
	public function setName( $value )
	{
		$this->_setValue( 'name', $value );
	}

	/**
	 * @return string VARCHAR(300) NULL
	 */
	public function getUrl()
	{
		return $this->_getValue('url');
	}

	/**
	 * @param string $value VARCHAR(300) NULL
	 */
	public function setUrl( $value )
	{
		$this->_setValue( 'url', $value );
	}

	/**
	 * @param bool $returnArray
	 * @return array|string JSONB NULL
	 */
	public function getSettings( $returnArray = true )
	{
		if ( $returnArray ) {
			if ( null === $this->_settings ) {
				$value = $this->_getValue('settings');
				if ( null === $value || '' === $value ) {
					$this->_settings = [];
				} else {
					$this->_settings = json_decode( $value, true ) ?: [];
				}
			}
			return $this->_settings;
		} else {
			return $this->_getValue('settings');
		}
	}

	/**
	 * @param array|string $value JSONB NULL
	 */
	public function setSettings( $value )
	{
		$this->_settings = null;
		if ( is_array( $value ) ) {
			$this->_settings = $value;
			$value = json_encode( $value );
		}
		$this->_setValue( 'settings', $value );
	}

	/**
	 * @return string 0 DEFAULT: pending
	 */
	public function getStatus()
	{
		return $this->_getValue('status');
	}

	/**
	 * @param string $value 0 DEFAULT: pending
	 */
	public function setStatus( $value )
	{
		$this->_setValue( 'status', $value );
	}

	/**
	 * @return string DATETIME
	 */
	public function getCreatedAt()
	{
		return $this->_getValue('created_at');
	}

	/**
	 * @param string $value DATETIME
	 */
	public function setCreatedAt( $value )
	{
		$this->_setValue( 'created_at', $value );
	}

	/**
	 * @return string DATETIME
	 */
	public function getUpdatedAt()
	{
		return $this->_getValue('updated_at');
	}

	/**
	 * @param string $value DATETIME
	 */
	public function setUpdatedAt( $value )
	{
		$this->_setValue( 'updated_at', $value );
	}

	/**
	 * @return int INT(32) NULL
	 */
	public function getCreatorId()
	{
		return $this->_getValue('creator_id');
	}

	/**
	 * @param int $value INT(32) NULL
	 */
	public function setCreatorId( $value )
	{
		$this->_setValue( 'creator_id', $value );
	}

	/**
	 * @param bool $returnArray
	 * @return array|string JSONB NULL
	 */
	public function getUrlStatus( $returnArray = true )
	{
		if ( $returnArray ) {
			if ( null === $this->_urlStatus ) {
				$value = $this->_getValue('url_status');
				if ( null === $value || '' === $value ) {
					$this->_urlStatus = [];
				} else {
					$this->_urlStatus = json_decode( $value, true ) ?: [];
				}
			}
			return $this->_urlStatus;
		} else {
			return $this->_getValue('url_status');
		}
	}

	/**
	 * @param array|string $value JSONB NULL
	 */
	public function setUrlStatus( $value )
	{
		$this->_urlStatus = null;
		if ( is_array( $value ) ) {
			$this->_urlStatus = $value;
			$value = json_encode( $value );
		}
		$this->_setValue( 'url_status', $value );
	}

	public function table()
	{
		return \Mobiteasy\Model::getOffer();
	}
}