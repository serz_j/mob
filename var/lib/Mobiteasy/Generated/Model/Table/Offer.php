<?php

namespace Mobiteasy\Generated\Model;

use \Mobiteasy\Model_Record_Offer;

/**
 * @method static Model_Record_Offer loadBy(array $conditions)
 * @method static Model_Record_Offer create()
 * @method static Model_Record_Offer[] findBy(array $conditions, $rawSql = '')
 * @method static Model_Record_Offer[] findAll($rawSql = '')
 * @method static Model_Record_Offer[] loadByQuery(\Connect\Database\Query $query)
 */
abstract class Table_Offer extends \Connect\Database\ActiveRecord\Table
{
	protected static $_recordClassName = '\\Mobiteasy\\Model_Record_Offer';

	protected static $_schema = 'mobiteasy';

	protected static $_table = 'offer';

	/** @var array */
	protected static $_metadata = array (
	  'id' =>
	  array (
	    'default' => NULL,
	    'type' => 8,
	    'null' => true,
	    'autoincrement' => true,
	    'primary' => 1,
	    'seq_name' => 'offer_id_seq',
	  ),
	  'old_id' =>
	  array (
	    'default' => NULL,
	    'type' => 8,
	    'null' => true,
	    'autoincrement' => false,
	    'primary' => false,
	  ),
	  'partner_id' =>
	  array (
	    'default' => '',
	    'type' => 8,
	    'null' => false,
	    'autoincrement' => false,
	    'primary' => false,
	  ),
	  'traffic_type_id' =>
	  array (
	    'default' => '',
	    'type' => 8,
	    'null' => false,
	    'autoincrement' => false,
	    'primary' => false,
	  ),
	  'manager_id' =>
	  array (
	    'default' => '',
	    'type' => 8,
	    'null' => false,
	    'autoincrement' => false,
	    'primary' => false,
	  ),
	  'name' =>
	  array (
	    'default' => NULL,
	    'type' => 2,
	    'null' => true,
	    'autoincrement' => false,
	    'primary' => false,
	    'length' => 255,
	  ),
	  'url' =>
	  array (
	    'default' => NULL,
	    'type' => 2,
	    'null' => true,
	    'autoincrement' => false,
	    'primary' => false,
	    'length' => 300,
	  ),
	  'settings' =>
	  array (
	    'default' => NULL,
	    'type' => 4,
	    'null' => true,
	    'autoincrement' => false,
	    'primary' => false,
	  ),
	  'status' =>
	  array (
	    'default' => 'pending',
	    'type' => 0,
	    'null' => false,
	    'autoincrement' => false,
	    'primary' => false,
	  ),
	  'created_at' =>
	  array (
	    'default' => '',
	    'type' => 3,
	    'null' => false,
	    'autoincrement' => false,
	    'primary' => false,
	  ),
	  'updated_at' =>
	  array (
	    'default' => '',
	    'type' => 3,
	    'null' => false,
	    'autoincrement' => false,
	    'primary' => false,
	  ),
	  'creator_id' =>
	  array (
	    'default' => NULL,
	    'type' => 8,
	    'null' => true,
	    'autoincrement' => false,
	    'primary' => false,
	  ),
	  'url_status' =>
	  array (
	    'default' => NULL,
	    'type' => 4,
	    'null' => true,
	    'autoincrement' => false,
	    'primary' => false,
	  ),
	);

	/** @var array */
	protected static $_primary = array (
	  0 => 'id',
	);

	protected static $metadata;

	public function load( $id )
	{
		return new \Mobiteasy\Model_Record_Offer( $id );
	}

	public function getMetadata()
	{
		if ( ! isset( self::$metadata ) )
		{
			self::$metadata = new \Setor\Connect\Database\ActiveRecord\Record_Metadata( self::$_schema, self::$_table, self::$_metadata, self::$_primary );
			self::$metadata->setRecordClassName( self::$_recordClassName );
		}
		return self::$metadata;
	}
}