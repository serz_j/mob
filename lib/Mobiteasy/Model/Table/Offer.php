<?php

namespace Mobiteasy;

class Model_Table_Offer extends \Mobiteasy\Generated\Model\Table_Offer
{
	public function getOfferStatuses ()
	{
		return [
			Model_Record_Offer::STATUS_ACTIVE,
			Model_Record_Offer::STATUS_PENDING,
			Model_Record_Offer::STATUS_DELETED
		];
	}

	/**
	 * @param int      $offerOldId
	 * @param int|null $trafficTypeId
	 * @param bool     $strictMode
	 *
	 * @return Model_Record_Offer
	 */
	public function loadByOldOfferId( $offerOldId, $trafficTypeId = null, $strictMode = false )
	{
		$offers = [];

		if ( ! $offerOldId )
		{
			return new Model_Record_Offer(); // Offer not found
		}

		foreach ( self::findBy(['old_id' => $offerOldId]) as $Offer )
		{
			$offers[ $Offer->getTrafficTypeId() ] = $Offer;
		}
		
		if ( ! $offers )
		{
			return new Model_Record_Offer(); // Offer not found
		}
		
		if ( $trafficTypeId && isset( $offers[ $trafficTypeId ] ) )
		{
			return $offers[ $trafficTypeId ];
		}
		else
		{
			if ( $strictMode && $trafficTypeId )
			{
				return new Model_Record_Offer(); // Offer not found
			}
			else
			{
				return reset( $offers );
			}
		}
	}

	public function getItemsForSelect()
	{
		return $this->_getDb()->execute( "
			SELECT
				id, name
			FROM
				offer
			ORDER BY
				name
		" )->fetchAllKeyValue();
	}

	/**
	 * @param  int $offerId
	 * @return int $traffic_type_id
	 */

	public function getOfferTrafficTypeId( $offerId )
	{
		return $this->_getDb()->execute( "
			SELECT
				traffic_type_id
			FROM
				offer
			WHERE status =:status AND id=:id
		",array(
			':status' => Model_Record_Offer::STATUS_ACTIVE,
			':id' => $offerId
		) )->fetchInt();
	}
}