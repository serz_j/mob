<?php

namespace Mobiteasy;

class Model_Record_Offer extends \Mobiteasy\Generated\Model\Record_Offer
{
	const STATUS_ACTIVE  = 'active';
	const STATUS_PENDING = 'pending';
	const STATUS_DELETED = 'deleted';

	public function isActive()
	{
		return self::STATUS_ACTIVE == $this->getStatus();
	}

	/**
	 * @return Model_Record_Partner
	 */
	public function getPartner()
	{
		return Model::loadPartner( $this->getPartnerId() );
	}

	static function getStatuses()
	{
		return [
			self::STATUS_ACTIVE  => self::STATUS_ACTIVE,
			self::STATUS_PENDING => self::STATUS_PENDING,
			self::STATUS_DELETED => self::STATUS_DELETED,
		];
	}

	/**
	 * @return Tracker_Offer
	 */
	public function getTrackerOffer()
	{
		$Offer = new Tracker_Offer();
		$Offer->setId($this->getId());

		$settings = $this->getSettings();
		if (!empty($settings['rules']) && is_array($settings['rules'])) {
			foreach ($settings['rules'] as $rules) {
				$rules = self::prepareRules($rules);
				if (!empty($rules)) {
					$OfferRule = new Tracker_OfferRule($rules);
					$Offer->addRule($OfferRule);
				}
			}
		}

		return $Offer;
	}


	public function getAndFixSettings()
	{
		return self::fixSettings($this->getSettings());
	}

	public function setSettings($value)
	{
		parent::setSettings(self::fixSettings($value));
	}

	public static function fixSettings(array $settings)
	{
		if (!empty($settings['rules']) && is_array($settings['rules'])) {
			$saveRules = [];
			foreach ($settings['rules'] as $rules) {
				$rules = self::prepareRules($rules, true);
				if (!empty($rules)) {
					$saveRules[] = $rules;
				}
			}

			$settings['rules'] = $saveRules;
		}

		return $settings;
	}

	private static function prepareRules(array $rules, $preserveCustomFilter = false)
	{
		static $checkDictionaryIdCache = [];

		$return = [];
		if (isset($rules['custom_filter'])) {
			$checkIds = [
				Tracker_OfferRule::RULE_SUBID1  => Tracker_OfferRule::RULE_SUBID1,
				Tracker_OfferRule::RULE_SUBID2  => Tracker_OfferRule::RULE_SUBID2,
				Tracker_OfferRule::RULE_HOST    => Tracker_OfferRule::RULE_HOST,
				Tracker_OfferRule::RULE_REFERER => Tracker_OfferRule::RULE_REFERER,
			];

			$customFilter = [];
			foreach ($rules['custom_filter'] as $filter) {
				$by    = $filter['filter_by'] ?? null;
				$value = $filter['filter_by_value'] ?? null;

				if (!$by) {
					continue;
				}

				$by    = trim($by);
				$value = trim($value);

				if (!isset($checkIds[ $by ])) {
					continue;
				}

				if ($preserveCustomFilter) {
					$customFilter[] = [
						'filter_by'       => $by,
						'filter_by_value' => $value,
					];
				} else {
					$return[ $by ][] = $value;
				}
			}

			if ($preserveCustomFilter) {
				$rules['custom_filter'] = $customFilter;
			} else {
				unset($rules['custom_filter']);
			}
		}

		$checkIds = [
			Tracker_OfferRule::RULE_OS      => Model_Record_Dictionary::ID_OS_FAMILY,
			Tracker_OfferRule::RULE_BROWSER => Model_Record_Dictionary::ID_BROWSER_FAMILY,
			Tracker_OfferRule::RULE_CARRIER => Model_Record_Dictionary::ID_CARRIER,
			Tracker_OfferRule::RULE_DEVICE  => Model_Record_Dictionary::ID_DEVICE_TYPE,
			Tracker_OfferRule::RULE_ISP     => Model_Record_Dictionary::ID_ISP,
			Tracker_OfferRule::RULE_COUNTRY => Model_Record_Dictionary::ID_COUNTRY,
		];

		foreach ($rules as $ruleName => $ruleValue) {
			if (isset($checkIds[ $ruleName ])) {
				$validatedRuleValue = [];
				$dictionaryId       = (int) $checkIds[ $ruleName ];
				if (!isset($checkDictionaryIdCache[ $ruleName ])) {
					$checkDictionaryIdCache[ $ruleName ] = [];
				}

				$checkValues = is_array($ruleValue) ? $ruleValue : [$ruleValue];
				foreach ($checkValues as $valueId) {
					if (!isset($checkDictionaryIdCache[ $ruleName ][ $valueId ])) {
						$DictionaryItem = Model::loadDictionaryItem($valueId);
						$isValid        = $DictionaryItem->isLoadedFromDb() && $DictionaryItem->getDictionaryId() == $dictionaryId;

						$checkDictionaryIdCache[ $ruleName ][ $valueId ] = $isValid;
					}

					if (!is_int($valueId)) {
						Context::logMessage("Invalid rule type: {$ruleName}, value id: {$valueId}, value type: " . gettype($valueId));
					}

					if ($checkDictionaryIdCache[ $ruleName ][ $valueId ]) {
						$validatedRuleValue[] = (int) $valueId;
					}
				}

				$ruleValue = $validatedRuleValue;
			}

			if (is_array($ruleValue) && empty($ruleValue)) {
				continue;
			} elseif (null === $ruleValue || '' === $ruleValue) {
				continue;
			}

			$return[ $ruleName ] = $ruleValue;
		}

		return $return;
	}

	/**
	 * @param int $countryId
	 *
	 * @return array|null
	 */
	public function getCountryPayout($countryId)
	{
		$settings = $this->getSettings();

		if (empty($settings['payouts']) || !is_array($settings['payouts'])) {
			return null;
		}

		$offerPayout = [];
		foreach ($settings['payouts'] as $payout) {
			if (empty($payout) || !is_array($payout) || empty($payout['country']) || !is_array($payout['country'])) {
				continue;
			}

			if (in_array($countryId, $payout['country']) && !empty($payout['payout']) && !empty($payout['currency'])) {
				$offerPayout['amount']   = $payout['payout'];
				$offerPayout['currency'] = $payout['currency'];
			}
		}

		return $offerPayout ?: null;
	}

	public function getIncludeRules()
	{
		$settings = $this->getSettings();
		$filters  = [];
		foreach ($settings['rules'] ?? [] as $rule) {
			if ($rule['include'] !== 'include') {
				continue;
			}

			if (empty($rule['country'])) {
				continue;
			}

			if (!isset($filters['country'])) {
				$filters['country'] = [];
			}

			$filters['country'] = array_unique(array_merge($filters['country'], $rule['country']));
		}

		return $filters;
	}

	public function getPushOffer()
	{
		$settings = $this->getSettings();
		$push_offer = false;
		$today = date('Y-m-d H:i:s');

		if(isset($settings['push_offer']) && $settings['push_offer']['push'])
		{
			if(strtotime($settings['push_offer']['end_time']) > strtotime($today)){
				$push_offer = true;
			}
		}

        return $push_offer;
    }

    public function getAffiliateAmount(Tracker_Click $click)
    {
        $amount = null;
        if ($this->isActive()) {

            $maxCount = 0;
            foreach ($this->getAffiliatePayout() as $payout) {
                if (!$count = $this->isMatchOffer($click, $payout)) {
                    continue;
                }
                if ($count > $maxCount) {
                    $amount = Currency_Rates::convert(str_replace(',', '.', $payout['payout']), $payout['currency']);;
                    $maxCount = $count;
                }
            }
            if (!is_null($amount)) {
                Context::saveLogMessage(get_called_class(), [
                  'amount'               => $amount,
                  'maxCount'             => $maxCount,
                  'offer'                => $this->getId(),
                  'affiliate'            => $click->affiliateId,
                  'country'              => $click->countryId,
                  'carrier'              => $click->carrierId,
                  'os'                   => $click->osFamilyId,
                  'traffic_compensation' => $click->trafficCompensationId,
                  'payout'               => $this->getAffiliatePayout(),
                ]);
            }
        }
        return $amount;
    }

    private function isMatchOffer(Tracker_Click $click, $payout)
    {
        $params = [
          'affiliateId' => 'affiliate',
          'countryId'   => 'country',
          'osFamilyId'  => 'os',
          'carrierId'   => 'carrier',
        ];
        $count = 0;
        foreach ($params as $key => $param) {
            if (isset($payout[$param]) && !empty($payout[$param])) {
                if (!in_array($click->$key, $payout[$param])) {
                    return false;
                }
                ++$count;
            }
        }
        return $count;
    }

    public function getAffiliatePayout()
    {
        $setting = $this->getSettings();
        return isset($setting['affiliatePayouts']) ? $setting['affiliatePayouts'] : [];
    }
}