mobiteasyApp.config(function ($interpolateProvider, $locationProvider) {
	$interpolateProvider.startSymbol('[[');
	$interpolateProvider.endSymbol(']]');
	$locationProvider.html5Mode(true).hashPrefix('!');
});
mobiteasyApp.controller('EditOfferController', function ($scope, $location, ApiService, ListsService, toaster, $filter) {
    function isEmpty(value) {
        return (angular.isUndefined(value) || value.length === 0 || value === '');
    }
	var counter = 0;

	$scope.weekdays = [
		{
			"id"  : 1,
			"name": "Monday"
		},
		{
			"id"  : 2,
			"name": "Tuesday"
		},
		{
			"id"  : 3,
			"name": "Wednesday"
		},
		{
			"id"  : 4,
			"name": "Thursday"
		},
		{
			"id"  : 5,
			"name": "Friday"
		},
		{
			"id"  : 6,
			"name": "Saturday"
		},
		{
			"id"  : 7,
			"name": "Sunday"
		}
	];

    $scope.device_type = [
        {id: 'All', name: 'All'},
        {id: 'Mobile', name: 'Mobile'},
        {id: 'Desktop', name: 'Desktop'}];

	$scope.timepickerOptions = {
		step      : 60,
		timeFormat: 'H:i',
		appendTo  : 'body',
		noneOption: [
	        {
		        label: '--clear--',
		        value: ''
	        }
        ]
	};

	$scope.offer = {};

	var ruleTemplate = {
		"include"      : 'include',
		"noweight"     : 0,
		"country"      : [],
		"device"       : [],
		"os"           : [],
		"partner"      : [],
		"traffic_type" : [],
		"isp"          : [],
		"carrier"      : [],
		"browser"      : [],
		"affiliate"    : [],
		"traffic_limit": '',
		"device_type"  : '',
		"custom_filter": [
            {
                'filter_by': '',
                'filter_by_value': ''
            }
        ]
	};

	$scope.custom_filter = ['subid1', 'subid2', 'referer', 'host'];

    var payoutTemplate = {
        'country': [],
        'payout': '',
        'currency': 'USD'
    };
    var affiliatePayoutTemplate = {
        'affiliate': [],
        'payout': '',
        'currency': 'USD',
        'country' : [],
        'os' : [],
        'carrier' : []
    };

    $scope.offer.settings = {
        "traffic_limit": '',
        "rules": [angular.copy(ruleTemplate)],
        "payouts": [angular.copy(payoutTemplate)],
        "affiliatePayouts": [angular.copy(affiliatePayoutTemplate)]
    };

    $scope.isp = data.isp;
    var ispIdList = [];
    angular.forEach($scope.isp, function (isp) {
        ispIdList[isp.id] = isp;
    });
    $scope.carrier = data.carrier;
    var carrierIdList = [];
    angular.forEach($scope.carrier, function (carrier) {
        carrierIdList[carrier.id] = carrier;
    });
    $scope.country = data.countries;
    $scope.countries = [];
    angular.forEach($scope.country, function (country) {
        $scope.countries[country.id] = country;
    });
    $scope.tierList = [];
    ApiService.send('offers', 'deleted', 'tier').then(function (response) {
        $scope.tierList = response;
    }, function (message) {
        console.log("error", "Ошибка", message, 3000);
    });
    var mobile_types = [], desktop_types = [], devices = [];
    ApiService.send('common', 'lists', 'device').then(function (response) {
        devices = response.list;
    }, function (message) {
        console.log("error", "Ошибка", message, 3000);
    });
    ApiService.send('common', 'lists', 'desktopDeviceType').then(function (response) {
        desktop_types = response.list;
    });
    ApiService.send('common', 'lists', 'mobileDeviceType').then(function (response) {
        mobile_types = response.list;
    });
    $scope.oss = [];
    ApiService.send('common', 'lists', 'os').then(function (response) {
        $scope.oss = response.list;
    }, function (message) {
        console.log("error", "Ошибка", message, 3000);
    });

	$scope.partners = [];
	ApiService.send('common', 'lists', 'partner').then(function (response) {
		$scope.partners = response.list;
	}, function (message) {
		console.log("error", "Ошибка", message, 3000);
	});

	$scope.traffic_type = [];
	ApiService.send('common', 'lists', 'traffic').then(function (response) {
		$scope.traffic_type = response.list;
	}, function (message) {
		console.log("error", "Ошибка", message, 3000);
	});

	$scope.netspeed = [];
	ApiService.send('common', 'lists', 'netspeed').then(function (response) {
		$scope.netspeed = response.list;
	}, function (message) {
		console.log("error", "Ошибка", message, 3000);
	});

	$scope.statuses = [];
	ApiService.send('common', 'lists', 'status').then(function (response) {
		$scope.statuses = response.list;
	}, function (message) {
		console.log("error", "Ошибка", message, 3000);
	});

    $scope.browsers = [];
    ApiService.send('common', 'lists', 'browser').then(function (response) {
        $scope.browsers = response.list;
    }, function (message) {
        console.log("error", "Ошибка", message, 3000);
    });

	$scope.tags = [];
	ApiService.send('common', 'lists', 'tags').then(function (response) {
		$scope.tags = response.list;
	}, function (message) {
		console.log("error", "Ошибка", message, 3000);
	});

	$scope.affiliates = [];
	ListsService.getAffiliates().then(function (affiliates) {
		$scope.affiliates = affiliates;
	}, function (error) {
		console.log("error", "Ошибка", message, 3000);
	});

    $scope.campaigns = [];
    ApiService.send('common', 'lists', 'campaigns').then(function (response) {
        $scope.campaigns = response.list;
    }, function (message) {
        console.log("error", "Ошибка", message, 3000);
    });
    $scope.checkDeviceType = function (rule) {
        var deviceList = [];
        if (isEmpty(rule.device_type) || rule.device_type === 'All') {
            deviceList = devices;
        } else {
            if (rule.device_type === 'Mobile') {
                deviceList = mobile_types;
            }
            if (rule.device_type === 'Desktop') {
                deviceList = desktop_types;
            }
        }
        rule.deviceList = deviceList;
        return rule;
    };
    $scope.addSelectedItem = function (query) {
        return ++counter;
    };
    $scope.getCountryList = function () {
        var countryList = [];
        angular.forEach($scope.countries, function (country, key) {
            countryList.push(country);
        });
        countryList.push({'id': 'tier1', 'name': 'Tier1'});
        countryList.push({'id': 'tier2', 'name': 'Tier2'});
        countryList.push({'id': 'tier3', 'name': 'Tier3'});
        countryList.push({'id': 'all', 'name': 'ALL'});
        return countryList;
    };
    $scope.getRuleTemplate = function () {
        ruleTemplate.countryList = $scope.getCountryList();
        ruleTemplate = $scope.checkDeviceType(ruleTemplate);
        ruleTemplate = $scope.checkIspAndCarrierAvailability(ruleTemplate);
        return ruleTemplate;
    };
    $scope.getPayoutTemplate = function () {
        payoutTemplate.countryList = $scope.getCountryList();
        return payoutTemplate;
    };
    $scope.getAffiliatePayoutTemplate = function () {
      affiliatePayoutTemplate.countryList = $scope.getCountryList();
      affiliatePayoutTemplate = $scope.checkIspAndCarrierAvailability(affiliatePayoutTemplate);
      return affiliatePayoutTemplate;
    };

    $scope.checkIspAndCarrierAvailability = function (rule) {
        if (isEmpty(rule.country) || rule.country.length === $scope.country.length) {
            rule.ispList = $scope.isp;
            rule.carrierList = $scope.carrier;
        }
        else {
            var conditionMcc = [], ispIds = [], carrierIds = [], countryList = [], carrierList = [], ispList = [],
                isp = [], carrier = [];
            angular.forEach(rule.country, function (countryId) {
                if (countryId == 'all') {
                    angular.forEach($scope.country, function (country) {
                        countryList.push(country.id);
                    });
                } else {
                    if (angular.isDefined($scope.tierList[countryId])) {
                        angular.forEach($scope.tierList[countryId], function (id) {
                            countryList.push(id);
                        });
                    }
                    else {
                        countryList.push(countryId);
                    }
                }
            });
            angular.forEach(countryList, function (country) {
                if (!isEmpty($scope.countries[country]) && $scope.countries[country].mcc != null) {
                    angular.forEach($scope.countries[country].mcc, function (mcc) {
                        if (conditionMcc.indexOf(mcc) === -1) {
                            conditionMcc.push(mcc);
                        }
                    });
                }
            });

            angular.forEach($scope.isp, function (isp) {
                conditionMcc.filter(function (value) {
                    if (value == isp.mcc) {
                        ispList.push(isp);
                        ispIds.push(isp.id);
                    }
                });
            });
            if (!isEmpty(rule.isp)) {
                angular.forEach(rule.isp, function (ispId) {
                    ispIds.filter(function (id) {
                        if (ispId == id)
                            isp.push(id);
                    })
                });
                rule.isp = isp;
            }
            $scope.carrier.filter(function (carrier) {
                angular.forEach(ispIds, function (ispId) {
                    if (carrier.parent.indexOf(ispId.toString()) !== -1 && carrierIds.indexOf(carrier.id) === -1) {
                        carrierList.push(carrier);
                        carrierIds.push(carrier.id);
                    }
                });
            });
            if (!isEmpty(rule.carrier)) {
                angular.forEach(rule.carrier, function (carrierId) {
                    carrierIds.filter(function (id) {
                        if (carrierId == id)
                            carrier.push(id);
                    })
                });
                rule.carrier = carrier;
            }
            rule.ispList = ispList;
            rule.carrierList = carrierList;
        }
        rule = $scope.getTipMessages(rule);
        return rule;
    };

    $scope.getTipMessages = function (rule) {
        rule.ispTip = '';
        rule.carrierTip = '';
        if (!isEmpty(rule.carrier)) {
            var ispTip = [];
            angular.forEach(rule.carrier, function (carrierId) {
                angular.forEach(carrierIdList[carrierId].parent, function (parentId) {
                    rule.ispList.filter(function (isp) {
                        if (isp.id == parentId && ispTip.indexOf(parentId) === -1) {
                            if (isEmpty(rule.isp)) {
                                ispTip.push(ispIdList[isp.id].name);
                            } else if (rule.isp.indexOf(isp.id) === -1) {
                                ispTip.push(ispIdList[isp.id].name);
                            }
                        }
                    });
                });
            });
            rule.ispTip = isEmpty(ispTip) ? '' : 'Tip: ' + ispTip.join(', ');
        }
        if (!isEmpty(rule.isp)) {
            var carrierTip = [];
            angular.forEach(rule.isp, function (ispId) {
                angular.forEach($scope.carrier, function (carrier) {
                    if (carrier.parent.indexOf(ispId.toString()) !== -1) {
                        if (isEmpty(rule.carrier)) {
                            carrierTip.push(carrierIdList[carrier.id].name);
                        } else if (rule.carrier.indexOf(carrier.id) === -1) {
                            carrierTip.push(carrierIdList[carrier.id].name);
                        }
                    }
                });
            });
            rule.carrierTip = isEmpty(carrierTip) ? '' : 'Tip: ' + carrierTip.join(', ');
        }
        return rule;
    };

    $scope.changeCountry = function (rule, type) {
        rule.countryList = $scope.getCountryList();
        if (rule.country.length === $scope.country.length) {
            rule = $scope.allCountries(rule);
        } else {
            rule = $scope.reassignCountryFromTier(rule);
        }
        if (type === 'rule') {
            rule = $scope.checkIspAndCarrierAvailability(rule);
        }
        return rule;
    };
    $scope.allCountries = function (rule) {
        rule.countryList = [{'id': 'all', 'name': 'ALL'}];
        rule.country = ['all'];
        return rule;
    };
    $scope.reassignTierFromCountry = function (rule) {
        var new_countries = [];
        angular.forEach($scope.tierList, function (tierArray, tierIndex) {
            if (rule.country.length >= tierArray.length) {
                var new_country_list = angular.copy(rule.country);
                var new_countryList_list = angular.copy(rule.countryList);
                angular.forEach(tierArray, function (item) {
                    new_countryList_list.filter(function (value, key) {
                        if (value.id === item) {
                            new_countryList_list.splice(key, 1);
                        }
                    });
                    new_country_list.filter(function (value, key) {
                        if (value === item) {
                            new_country_list.splice(key, 1);
                        }
                    });
                });
                if (rule.country.length - tierArray.length === new_country_list.length) {
                    rule.country = new_country_list;
                    new_countries.push(tierIndex);
                    rule.countryList = new_countryList_list;
                }
            }
        });
        rule.country = new_countries.length === 0 ? rule.country :
            rule.country.concat(new_countries);
        return rule;
    };
    $scope.reassignCountryFromTier = function (rule) {
        var all = 0;
        var new_countries = [];
        angular.forEach(rule.country, function (countryId) {
            if (countryId == 'all') {
                return $scope.allCountries(rule);
            }
            angular.forEach($scope.tierList, function (tierArray, tierIndex) {
                if (countryId == tierIndex) {
                    all++;
                    if (all < 3) {
                        angular.forEach(tierArray, function (item) {
                            rule.countryList.filter(function (value, key) {
                                if (value.id === item) {
                                    rule.countryList.splice(key, 1);
                                }
                            });
                            rule.country.filter(function (value, key) {
                                if (value === item) {
                                    rule.country.splice(key, 1);
                                }
                            });
                        });
                        new_countries.push(tierIndex);
                    }
                }
            });
        });
        if (all == 3) {
            return $scope.allCountries(rule);
        }
        rule.country.concat(new_countries);
        return rule;
    };
    $scope.getNormalCountryList = function (rule) {
        if (isEmpty(rule.country)) {
            return rule;
        } else {
            rule.country.filter(function (countryId) {
                if (countryId == 'all') {
                    var country_all = [];
                    angular.forEach($scope.countries, function (countriesArray) {
                        country_all.push(countriesArray.id)
                    });
                    return rule.country = country_all;
                }
            });
            var new_countries = [];
            angular.forEach($scope.tierList, function (tierArray, tierIndex) {
                angular.forEach(rule.country, function (countryId, countryIndex) {
                    if (countryId == tierIndex) {
                        rule.country.splice(countryIndex, 1);
                        angular.forEach(tierArray, function (tierId) {
                            new_countries.push(tierId);
                        });
                    }
                })
            });
            rule.country = rule.country.length === 0 ? new_countries : rule.country.concat(new_countries);
            return rule;
        }
    };
    $scope.getModifiedCountryList = function (rule) {
        rule.countryList = $scope.getCountryList();
        if (isEmpty(rule.country)) {
            return rule;
        } else {
            if (rule.country.length === $scope.country.length) {
                return $scope.allCountries(rule);
            }
            else {
                rule.countryList = $scope.getCountryList();
                return $scope.reassignTierFromCountry(rule);
            }
        }
    };
    $scope.formatTime = function (t, toDate) {

		if (angular.isUndefined(t)) {
			return null;
		}

		if (angular.isDefined(toDate) && moment(t, "HH:mm").isValid()) {
			return moment(t, "HH:mm");
		}

		if (moment(t).isValid()) {
			return moment(t).format("HH:mm");
		}

        return null;
    };
    $scope.getDirectUrl = function () {
            ApiService.send('offers', 'deleted', 'direct', {traffic_type_id: $scope.offer.traffic_type_id}).then(function (response) {
                $scope.offer.directUrl = response.name;
            }, function (message) {
            console.log("error", "Ошибка", message, 3000);
        });
    };
    $scope.assignResponseData = function (response) {
        $scope.offer = response.offer;
        $scope.status = $scope.offer.status;
        $scope.offer.campaign_ids = response.offer.campaign_ids;
        $scope.offer.settings = !response.offer.settings || $.isEmptyObject(response.offer.settings) ? {
            // rules: [ruleTemplate],
            rules: [$scope.getRuleTemplate()],
            payouts: [$scope.getPayoutTemplate()],
            affiliatePayouts: [angular.copy($scope.getAffiliatePayoutTemplate())]
        } : angular.fromJson(response.offer.settings);

        if (response.offer.settings.targeting_time) {
            $scope.offer.settings.targeting_time.start = $scope.formatTime(response.offer.settings.targeting_time.start, true);
            $scope.offer.settings.targeting_time.end = $scope.formatTime(response.offer.settings.targeting_time.end, true);
        }

        angular.forEach($scope.offer.settings.rules, function (rule, key) {
            rule = $scope.getModifiedCountryList(rule);
            rule = $scope.checkDeviceType(rule);
            rule = $scope.checkIspAndCarrierAvailability(rule);
            if (!rule.custom_filter) {
                rule.custom_filter = [
                    {
                        'filter_by': '',
                        'filter_by_value': ''
                    }
                ];
            }
        });
        $scope.offer.settings.payouts = !$scope.offer.settings.payouts ? $scope.getPayoutTemplate() : $scope.offer.settings.payouts;
        angular.forEach($scope.offer.settings.payouts, function (payout) {
            payout = $scope.getModifiedCountryList(payout);
        });
        $scope.offer.settings.affiliatePayouts = !$scope.offer.settings.affiliatePayouts ? [angular.copy($scope.getAffiliatePayoutTemplate())] : $scope.offer.settings.affiliatePayouts;
        angular.forEach($scope.offer.settings.affiliatePayouts, function (payout) {
            payout = $scope.getModifiedCountryList(payout);
            payout = $scope.checkIspAndCarrierAvailability(payout);
        });
        $scope.offer.traffic_type_id = parseInt($scope.offer.traffic_type_id);
        $scope.getDirectUrl();
    };
    $scope.loadOffer = function (id, clone) {
        if (!id) {
            angular.forEach($scope.offer.settings.rules, function (rule, key) {
                rule.countryList = $scope.getCountryList();
                rule = $scope.checkDeviceType(rule);
                rule = $scope.checkIspAndCarrierAvailability(rule);
                if (!rule.custom_filter) {
                    rule.custom_filter = [
                        {
                            'filter_by': '',
                            'filter_by_value': ''
                        }
                    ];
                }
            });
            angular.forEach($scope.offer.settings.payouts, function (payout) {
                payout.countryList = $scope.getCountryList();
            });
            angular.forEach($scope.offer.settings.affiliatePayouts, function (payout) {
                payout.countryList = $scope.getCountryList();
                payout = $scope.checkIspAndCarrierAvailability(payout);
            });
            return;
        }

        ApiService.send('offers', 'offer', id).then(function (response) {
            $scope.assignResponseData(response)
        }, function (message) {
            console.log("error", "Ошибка", message, 3000);
        });

    };
    function sendToaster(status, message) {
        toaster.pop({
            type: status,
            body: message,
            showCloseButton: true
        });
    }

    $scope.isEmptyAffiliatePayout = function (affiliate_payout) {
        if (isEmpty(affiliate_payout.affiliate) && isEmpty(affiliate_payout.type) && isEmpty(affiliate_payout.payout) && $scope.isValidCurrency(affiliate_payout.currency)) {
            return true;
        } else {
            return false;
        }
    };

    $scope.checkAffiliatePayout = function () {
        var valid = true;
        angular.forEach($scope.offer.settings.affiliatePayouts, function (affiliate_payout) {
            if ($scope.offer.settings.affiliatePayouts.length === 1 && $scope.isEmptyAffiliatePayout(affiliate_payout)) {
                return valid;
            }
            if (isEmpty(affiliate_payout.affiliate)) {
                sendToaster('error', 'Required at least one affiliate in Static Affiliate Payout');
                valid &= false;
            }
            if(isEmpty(affiliate_payout.payout) || affiliate_payout.payout < 0){
                sendToaster('error', 'Required payout in Static Affiliate Payout');
                valid &= false;
            }
            if (!$scope.isValidCurrency(affiliate_payout.currency)) {
                sendToaster('error', 'Required currency like USD or EUR in Static Affiliate Payout');
                valid &= false;
            }
        });
        return valid;
    };
    $scope.isValidCurrency = function (currency) {
        return (currency === 'USD' || currency === 'EUR');
    };
    $scope.deleteBeforeSave = function (rule) {
        delete rule.countryList;
        rule = $scope.getNormalCountryList(rule);
        delete rule.carrierList;
        delete rule.ispList;
        delete rule.deviceList;
        delete rule.ispTip;
        delete rule.carrierTip;
        return rule;
    };
    $scope.saveOffer = function (id) {
        var offer_data = angular.copy($scope.offer);

        if ($scope.nameRequired) {
            return false;
        }
        angular.forEach(offer_data.settings.rules, function (rule, key) {
            rule = $scope.deleteBeforeSave(rule);
        });
        angular.forEach(offer_data.settings.payouts, function (payout) {
            payout = $scope.deleteBeforeSave(payout);
        });
        angular.forEach(offer_data.settings.affiliatePayouts, function (payout) {
            if (offer_data.settings.affiliatePayouts.length === 1 && $scope.isEmptyAffiliatePayout(payout)) {
                delete offer_data.settings.affiliatePayouts;
            } else {
                payout = $scope.deleteBeforeSave(payout);
            }
        });
        var valid = $scope.checkAffiliatePayout();

		if(angular.isDefined( offer_data.settings.push_offer ) )
		{
			if(offer_data.settings.push_offer.push){
				var date = new Date();
				date.setDate(date.getDate() +1);
				$scope.end_time = $filter('date')(new Date(date), 'yyyy-MM-dd HH:mm:ss');

				offer_data.settings.push_offer.end_time  = $scope.end_time;
			}else{
				offer_data.settings.push_offer.push = false;
				offer_data.settings.push_offer.end_time = '';
			}
		}

        if (offer_data.settings.targeting_time) {
            if (offer_data.settings.targeting_time.start == null) {
                delete offer_data.settings.targeting_time.start;
            } else {
                offer_data.settings.targeting_time.start = $scope.formatTime(offer_data.settings.targeting_time.start);
            }

            if (offer_data.settings.targeting_time.end == null) {
                delete offer_data.settings.targeting_time.end;
            } else {
                offer_data.settings.targeting_time.end = $scope.formatTime(offer_data.settings.targeting_time.end);
            }
        }
        if (valid) {
            ApiService.send('offers', 'offer', 'add', offer_data).then(function (response) {
                $scope.assignResponseData(response);
                if (response.result == 'added') {

				$location.path('/manager/offer/' + $scope.offer.id);

				toaster.pop({
					type           : 'success',
					body           : response.result,
					showCloseButton: true
				});
				//window.location.reload();
			} else if (response.result == 'saved') {
				console.log("Offer saved");
				toaster.pop({
					type           : 'success',
					body           : response.result,
					showCloseButton: true
				});
			} else if (response.message) {
				toaster.pop({
					type           : 'error',
					body           : response.message,
					showCloseButton: true
				});
			}

                //console.log("After save: ", $scope.offer);
            }, function (message) {
                toaster.pop({
                    type: 'error',
                    body: message,
                    showCloseButton: true
                });
                console.log(message);
            });
        }
    };

	$scope.$watch('offer.name', function () {
		$scope.checkName();
	}, true);

	$scope.checkName = function () {
		ApiService.send('offers', 'offer', 'name', $scope.offer).then(function (response) {
			if (response.result == 'error') {
				$scope.nameRequired = 'Name already exist!';
			} else {
				$scope.nameRequired = '';
			}

		});
	};

	$scope.cloneOffer     = function (id) {
		$scope.offer.clone = true;
		delete $scope.offer.id;

		history.pushState('', '', '/manager/offer/add');

		$('html,body').animate({
			scrollTop: 0
		}, 400);

		toaster.pop({
			type           : 'success',
			body           : 'created clone of: ' + $scope.offer.name,
			showCloseButton: true
		});

        $scope.offer.name = $scope.offer.name + ' clone';

	};

	$scope.addRule        = function () {
		$scope.offer.settings.rules.push(angular.copy($scope.getRuleTemplate()));
	};
	$scope.removeRule     = function (index) {
		if (index > 0) {
			$scope.offer.settings.rules.splice(index, 1);
		}
	};
	$scope.addCustomFilter = function(rule) {
        rule.custom_filter.push(angular.copy({
            'filter_by': '',
            'filter_by_value': ''
        }));
    };
    $scope.removeCustomFilter = function(rule, index) {
        if(index>0){
            rule.custom_filter.splice(index,1);
        }
    };
    $scope.addPayout = function () {
        $scope.offer.settings.payouts.push(angular.copy($scope.getPayoutTemplate()));
    };
    $scope.removePayout = function (index) {
        if (index > 0) {
            $scope.offer.settings.payouts.splice(index, 1);
        }
    };
    $scope.addAffiliatePayout = function () {
        $scope.offer.settings.affiliatePayouts.push(angular.copy($scope.getAffiliatePayoutTemplate()));
    };
    $scope.removeAffiliatePayout = function (index) {
        $scope.offer.settings.affiliatePayouts.splice(index, 1);
        if ($scope.offer.settings.affiliatePayouts.length === 0) {
            $scope.addAffiliatePayout();
        }
    };
    $scope.typeInTextarea = function (newText) {
        var el = angular.element("#tracker");
        var start = el.prop("selectionStart"),
            end = el.prop("selectionEnd"),
            text = el.val(),
            before = text.substring(0, start),
            after = text.substring(end, text.length);
        $scope.offer.url = before + newText + after;
        el.val(before + newText + after);
        el[0].selectionStart = el[0].selectionEnd = start + newText.length;
        el.focus();
    };

	$scope.assignToManager = function(id) {

		if (!id) return;

		if (!confirm("Are you realy want to assign this offer on yourself ?")) {
			return;
		}

		ApiService.send('offers', 'offer', 'assign', {"id":id}).then(function(response){

			$scope.offer.creator_id = response.creator_id;
			$location.path('/manager/offer/' + $scope.offer.id);

			if (response.result == 'assigned') {
				toaster.pop({
					type: 'success',
					body: 'Successfully assigned to you!',
					showCloseButton: true
				});
			} else {
				toaster.pop({
					type: 'error',
					body: 'Something went wrong!',
					showCloseButton: true
				});
			}
		});
	};

	$scope.changeState = function() {

		if(!confirm("Are you realy want to change status?")){
			$scope.offer.status = $scope.status;
		}
	};

}).filter('mySearchFilter', function ($sce) {
	return function (label, query, item, options, element) {

		var html = label + (item.legacy ? ' <kbd>legacy</kbd>' : '') + ' <span class="close select-search-list-item_selection-remove">×</span>';

		return $sce.trustAsHtml(html);
	};
}).filter('myDropdownFilter', function ($sce) {
	return function (label, query, item, options, element) {

		var html = label + (item.legacy ? ' <kbd>legacy</kbd>' : '');

		return $sce.trustAsHtml(html);
	};

});
