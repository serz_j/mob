mobiteasyApp.controller('ListOffersController', function($scope, ApiService, ListsService, toaster){

    $scope.traffic_type = [];
    $scope.offers = [];
    $scope.partners = [];
    $scope.search = {
        //'status': 'deleted'
    };
    $scope.affiliates = [];
    $scope.oss = [];

    $scope.init = function(){

        ApiService.send('offers', 'deleted', 'deleted').then(function(response){
            $scope.offers = response.offers;
        });

        ApiService.send('common', 'lists', 'partner').then(function(response){
            $scope.partners = response.list;
        });

        ApiService.send('common', 'lists', 'traffic').then(function(response){
            $scope.traffic_type = response.list;
        });

        ListsService.getAffiliates().then(function(affiliates){
            $scope.affiliates = affiliates;
        }, function(error){
            console.log("error", "Ошибка", message, 3000);
        });

        ApiService.send('common', 'lists', 'country').then(function(response){
            $scope.countries = response.list;
        }, function(message){
            console.log("error", "Ошибка", message, 3000);
        });

        ApiService.send('common', 'lists', 'os').then(function(response){
            $scope.oss = response.list;
        }, function(message){
            console.log("error", "Ошибка", message, 3000);
        });

        ApiService.send('common', 'lists', 'carrier').then(function (response) {
            $scope.carrier = response.list;
        }, function (message) {
            console.log("error", "Ошибка", message, 3000);
        });
    };

    $scope.$watch('search.affiliates', function () {
        $scope.exclude_search_filter();
    }, true);

    $scope.exclude_search_filter = function() {

        if($scope.search.affiliates){

            angular.forEach($scope.offers, function (exclude, key) {

                if(exclude.excludes.indexOf($scope.search.affiliates) !== -1) {
                    $scope.offers[key].highlighted = true;
                }
            });

        }else{
            angular.forEach($scope.offers, function (exclude, key) {
                $scope.offers[key].highlighted = false;
            });
        }
    };

    $scope.clearSearchParam = function(param){
        if (!param) {
            return;
        }
        delete $scope.search[param];
        delete $scope.reverse;
        delete $scope.sortByParam;
    };

    $scope.sortBy = function(propertyName) {
        $scope.reverse = ($scope.sortByParam === propertyName) ? !$scope.reverse : false;
        $scope.sortByParam = propertyName;

    };

});
