<?php

namespace Mobiteasy;

class Offers_Offers extends \Framework\Mvc\Model
{
    const TIER_1 = ['US','CA','AU','GB'];
    const TIER_2 = ['FR','DE','BE','IT','NL','TR','ES','AT','CH','FI','NO','SE','DK','JP','BR','EE','LV','LT',
                    'MX','LI','MC','LU','SG','IS','SM','AD','NZ','BS','CL','AR','PY','PE','UY','GR','BG','HU',
                    'IE','CY','MT','PT','PL','RO','SI','SK','CZ','HR','GE','IL','XK','MK','MA','ZA','AZ','RU'];

    public function getOffersList()
	{
		return $this->db->fetchAll( "
			SELECT
			  offer.id,
			  offer.name,
			  offer.status,
			  partner.name AS partner,
			  traffic_type.name AS type
			FROM
			  offer
			  LEFT OUTER JOIN partner ON (offer.partner_id = partner.id)
			  LEFT OUTER JOIN traffic_type ON (offer.traffic_type_id = traffic_type.id)
		" );

	}

}