<?php

namespace Mobiteasy;

use Framework\Mvc\Router_Rewrite;
use Framework\Mvc\Router_Route_Pattern;


class Offers_Routing
{
	/** @var Router_Rewrite */
	private $router;

	/**
	 * @param Router_Rewrite $Router
	 */
	public function __construct( Router_Rewrite $Router )
	{
		$this->router = $Router;
	}

	public function initialize()
	{
		$Router = $this->router->getRouter( Environment::END_MANAGER );

		$Router->addRoute( 'manager_offers', new Router_Route_Pattern( 'offers/active', array(
			'module'     => 'offers',
			'controller' => 'offers',
			'action'     => 'index',
		) ) );
		$Router->addRoute( 'offers_deleted', new Router_Route_Pattern( 'offers/deleted', array(
				'module'     => 'offers',
				'controller' => 'deleted',
				'action'     => 'index',
		) ) );
		$Router->addRoute( 'manager_offer_add', new Router_Route_Pattern( 'offer/add', array(
			'module'     => 'offers',
			'controller' => 'offer',
			'action'     => 'add',
		) ) );
		$Router->addRoute( 'manager_offer', new Router_Route_Pattern( 'offer/:id', array(
			'module'     => 'offers',
			'controller' => 'offer',
			'action'     => 'edit',
		) ) );

		$Router = $this->router->getRouter( Environment::END_API );
		$Router->addRoute( 'api_offer_list', new Router_Route_Pattern( 'offers/offer/list', array(
				'module'     => 'offers',
				'controller' => 'offer',
				'action'     => 'list',
		) ) );
		$Router->addRoute( 'api_offer_add', new Router_Route_Pattern( 'offers/offer/add', array(
				'module'     => 'offers',
				'controller' => 'offer',
				'action'     => 'add',
		) ) );
		$Router->addRoute( 'api_offer_state', new Router_Route_Pattern( 'offers/offer/state', array(
				'module'     => 'offers',
				'controller' => 'offer',
				'action'     => 'state',
		) ) );
		$Router->addRoute( 'api_offer_name', new Router_Route_Pattern( 'offers/offer/name', array(
				'module'     => 'offers',
				'controller' => 'offer',
				'action'     => 'checkName',
		) ) );
		$Router->addRoute( 'api_offer_assign', new Router_Route_Pattern( 'offers/offer/assign', array(
				'module'     => 'offers',
				'controller' => 'offer',
				'action'     => 'assignToManager',
		) ) );
		$Router->addRoute( 'api_offer', new Router_Route_Pattern( 'offers/offer/:id', array(
				'module'     => 'offers',
				'controller' => 'offer',
				'action'     => 'edit',
		) ) );
		$Router->addRoute( 'api_offer_tier', new Router_Route_Pattern( 'offers/deleted/tier', array(
				'module'     => 'offers',
				'controller' => 'deleted',
				'action'     => 'tierCountry',
		) ) );
		$Router->addRoute( 'api_offer_direct', new Router_Route_Pattern( 'offers/deleted/direct', array(
				'module'     => 'offers',
				'controller' => 'deleted',
				'action'     => 'directUrl',
		) ) );
	}
}