<?php

namespace Mobiteasy;

use Form\Form;


class Offers_Controller_Manager_Offer extends Controller_Manager
{
    public function executeEdit(array $params)
    {
        if (!empty($params["id"])) {
            $offer = Model::loadOffer($params["id"]);
            $this->assign("offer", $offer);
        }
        $this->assign('tier1', implode(', ', Offers_Offers::TIER_1));
        $this->assign('tier2', implode(', ', Offers_Offers::TIER_2));
        $ispList = [];
        foreach (Context::redis()->hGetAll(Model_Record_Dictionary::REDIS_ISP_LIST) as $isp) {
            $ispList[] = json_decode($isp, true);
        }
        $carrierList = [];
        foreach (Context::redis()->hGetAll(Model_Record_Dictionary::REDIS_CARRIER_LIST) as $carrier) {
            $carrierList[] = json_decode($carrier, true);
        }
        $this->assign('countries', Model::getDictionary()->getCountryListForApi());
        $this->assign('isp', $ispList);
        $this->assign('carrier', $carrierList);
        $Form = $this->createAssignForm("form", Form::METHOD_GET);
        $Form->addSubmit('submit', 'Save offer');

        if ($this->isFormSubmittedAndValid($Form)) {
            print_info($Form->getValues());
        }
    }

    public function executeAdd()
    {
        $this->forward("edit");
    }
}