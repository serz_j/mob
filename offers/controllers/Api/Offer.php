<?php

namespace Mobiteasy;
use Connect\Database\QueryCondition;


class Offers_Controller_Api_Offer extends Controller_Api
{
	public function executeIndex()
	{
		$this->getAjaxResponse()->setFail();
	}

	public function executeList()
	{
		$this->requireManager();

		$response = [];


        $Condition = new QueryCondition();
        $Condition->where = 'status != :status';
        $Condition->bind = [
            ':status' => Model_Record_Offer::STATUS_DELETED,
        ];

        $findBy['status'] = $Condition;

        $offers   = Model::getOffer()->findBy($findBy, 'ORDER BY id ASC');

	//campaigns:  this is for check if the offer in campaign
		$campaignsOffers = [];
		foreach(Model::getCampaign()->findAll() as $campaign)
		{
			$campaignSettings = $campaign->getSettings();

			$offer_ids = [];
			if(isset($campaignSettings['offers']) ){
				foreach($campaignSettings['offers'] as $offer_id){
					$offer_ids[] = $offer_id['id'] ?? '';
				}
			}

			$campaignsOffers[$campaign->getId()] = $offer_ids;
		}
	//end campaigns

		$offerCpm = new Offers_OfferCpm();
		$cpm      = $offerCpm->getOffersCpmByDate(date("Y-m-d"));

		$cpmList = [];
		foreach ($cpm["lines"] as $row) {
			if (empty($row["network_ecpm"])) {
				continue;
			}
			$cpmList[ $row["id"] ] = $row["network_ecpm"];
		}

		foreach ($offers as $k => $offer) {
			$settings    = $offer->getSettings();
			$affiliates  = [];
			$countries   = [];
			$device_type = '';
			$oss         = [];
			$excludes    = [];
			$exclude     = [];
            $carriers    = [];

			if ( isset($settings['rules']) )
			{
				foreach ($settings['rules'] as $rules)
				{
					$affiliate  = $rules['affiliate'] ?? [];
					$country    = $rules['country'] ?? [];
					$os         = $rules['os'] ?? [];
                    $carrier    = $rules['carrier'] ?? [];

					if( isset($rules['include']) && $rules['include'] == 'exclude' )
					{
						$exclude = $rules['affiliate'] ?? [];
					}

					$affiliates = array_merge( $affiliate, $affiliates );
					$countries  = array_merge( $country, $countries );
					$oss        = array_merge( $os, $oss );
					$excludes   = array_merge( $exclude, $excludes );
                    $carriers   = array_merge( $carrier, $carriers );

					$device_type = $rules['device_type'] ?? '';

				}
			}


			$offerData                 = $offer->_getData();
			$offerData['affiliates']   = array_values( array_unique( $affiliates ) );
			$offerData['countries']    = $countries;
			$offerData['device_type']  = $device_type;
            $offerData['carrier']      = $carriers;
			$offerData['excludes'] 	   = array_values( array_unique( $excludes ) );
			$offerData['oss']          = $oss;
			$offerData["type"]         = Model::loadTrafficType($offer->getTrafficTypeId())->getName();
			$offerData['partner_name'] = Model::loadPartner($offer->getPartnerId())->getName();
			$offerData['updated_by']   = Model::loadManager($offer->getManagerId())->getLogin();
			$offerData['created_by']   = empty( $offer->getCreatorId() ) ? 'NONE' : Model::loadManager( $offer->getCreatorId())->getLogin();
			$offerData['cpm_by_date']  = empty($cpmList[ $offerData["id"] ]) ? [] : $cpmList[ $offerData["id"] ];

			//offer campaigns
			$offerData['campaigns'] = [];
			foreach($campaignsOffers as $camp_id => $campOffers){

				if(in_array($offer->getId(), $campOffers)){
					$offerData['campaigns'][] =  $camp_id;
				}
			}

			$response['offers'][]      = $offerData;
		}
		$this->getAjaxResponse()->setSuccess($response);
	}

	public function executeEdit(array $params)
	{
		$this->requireManager();

		$response = [];

		if (empty($params["id"])) {
			$offerData = $this->request->getInputJson();
			$this->db->start();

			if (!empty($offerData["id"])) {
				$offer = Model::loadOffer($offerData['id']);
			} else {
				$offer = new Model_Record_Offer();
			}

			$response['result'] = "saved";

			if ($offer->isNew() || !empty($offerData["clone"])) {
				$response['result'] = "added";
				$offer->setCreatorId($this->getManager()->getId());

				//find excluded affiliates in partner and create exclude rule in offer
				$Partner 		 = Model::loadPartner(intval( $offerData["partner_id"] ));
				$partnerSettings = $Partner->getSettings();

				if( isset( $partnerSettings['excludeAffil'] ) && ! empty( $partnerSettings['excludeAffil'] && empty( $offerData['clone'] ) ) ){
					$excludeRule = [
							'include'     => 'exclude',
							'exclude'     => true,
							'noweight'    => 0,
							'device_type' => 'All',
							'affiliate'   => $partnerSettings['excludeAffil']
					];
					$offerData["settings"]['rules'][] = $excludeRule;
				}
			}

			$offer->setManagerId($this->getManager()->getId());
			$offer->setPartnerId( intval( $offerData["partner_id"] ) );
			$offer->setUrl($offerData["url"]);
			$offer->setName($offerData["name"]);
			$offer->setTrafficTypeId((integer) $offerData["traffic_type_id"]);
			$offer->setStatus($offerData["status"]);

			if (!empty($offerData["settings"])) {
				$offer->setSettings($offerData["settings"]);
			}

			$offer->save();

			// add/delete offer in/from campaign
			$offerData['id']           = $offerData['id'] ?? $offer->getId();
			$offerData['campaign_ids'] = $offerData['campaign_ids'] ?? [];

			$rules = [
				'id'     => $offerData['id'],
				'rules'  => [
					'country'       => [],
					'custom_filter' => [
						0 => [
							'filter_by'       => '',
							'filter_by_value' => '',
						],
					],
				],
				'weight' => 100,
				'active' => true,
				'force'  => [
					'active' => 'no',
					'limit'  => 0,
				],
			];

			foreach (Model::getCampaign()->findAll() as $campaignsData) {
				$campaignsDataId   = $campaignsData->getId();
				$campaignsSettings = $campaignsData->getSettings();
				$campaignsData->setManagerId($this->getManager()->getId());

				$campaignOfferIds = [];
				foreach ($campaignsSettings['offers'] as $campaignOffers) {
					$campaignOfferIds[] = $campaignOffers['id'];
				}

				if (($offerData["status"] != $offer::STATUS_ACTIVE || array_search($campaignsDataId, $offerData['campaign_ids']) === false) && in_array($offerData['id'], $campaignOfferIds)) {
					foreach ($campaignsSettings['offers'] as $k => $offers) {
						if ($offers['id'] == $offerData['id']) {
							unset($campaignsSettings['offers'][ $k ]);
						}
					}

					$campaignsData->setSettings($campaignsSettings);
					$campaignsData->save();
				} else {
					if (array_search($offerData['id'], $campaignOfferIds) === false && in_array($campaignsDataId, $offerData['campaign_ids']) && $offerData["status"] == $offer::STATUS_ACTIVE) {
						$campaignsSettings['offers'][] = $rules;
						$campaignsData->setSettings($campaignsSettings);
						$campaignsData->save();
					}
				}
			}

			$this->db->commit();

			$params['id'] = $offer->getId();
		}

		$campaign_ids = [];
		foreach (Model::getCampaign()->findAll() as $campaigns) {
			$settings = $campaigns->getSettings();
			foreach ($settings['offers'] as $offer_ids) {
				if ($offer_ids['id'] == $params['id']) {
					$campaign_ids[] = $campaigns->getId();
				}
			}
		}

		$Offer = Model::loadOffer($params['id']);

		$response['offer']                		  = $Offer->_getData();
		$response['offer']['url_status']        = $Offer->modifyUrlStatus();
		$response['offer']['settings']    		  = $Offer->getAndFixSettings();
		$response['offer']['campaign_ids'] 		  = $campaign_ids;
		$response['offer']['current_manager_id']  = $this->getManager()->getId();

		if (empty($response['offer'])) {
			$this->getAjaxResponse()->setFail($response);
		} else {
			$this->getAjaxResponse()->setSuccess($response);
		}
	}

	public function executeAdd()
	{
		$this->forward('edit');
	}

	public function executeState()
	{
		$this->requireManager();

		$response  = [];
		$offerData = $this->request->getInputJson();

		if (!empty($offerData["id"])) {
			$offer = Model::loadOffer($offerData["id"]);
		} else {
			$offer = Model::getOffer()->create();
		}

		if ($offer->isNew()) {
			$response['message'] = 'check offer ID!';
			$this->getAjaxResponse()->setFail($response);

			return;
		}

		$offer->setStatus($offerData["status"]);
		$offer->setManagerId($this->getManager()->getId());

		if ($offerData["status"] != $offer::STATUS_ACTIVE) {
			$status = false;
			foreach (Model::getCampaign()->findAll() as $campaignsData) {
				$campaignsSettings = $campaignsData->getSettings();
				foreach ($campaignsSettings['offers'] as $k => $offers) {
					if ($offers['id'] == $offerData["id"]) {
						unset($campaignsSettings['offers'][ $k ]);
						$status = true;
					}
				}

				if ($status) {
					$campaignsData->setSettings($campaignsSettings);
					$campaignsData->save();
					$status = false;
				}
			}
		}

		$offer->save();

		$response['offer']   = $offer->_getData();
		$response['message'] = 'Offer state changed!';

		$this->getAjaxResponse()->setSuccess($response);
	}

	public function executeCheckName()
	{
		$this->requireManager();

		$offerData         = $this->request->getInputJson();
		$response          = [];
		$offerData['name'] = $offerData['name'] ?? '';
		$offerData['id']   = $offerData['id'] ?? '';

		if (!empty($offerData['id'])) {
			$offer = Model::loadOffer($offerData['id']);

			if ($offer->getName() != $offerData['name']) {
				$offer = Model::getOffer()->loadBy([
					'name' => $offerData['name'],
				]);
				if (!$offer->isNew()) {
					$response['result'] = 'error';
				}
			}
		} else {
			$offer = Model::getOffer()->loadBy([
				'name' => $offerData['name'],
			]);
			if (!$offer->isNew()) {
				$response['result'] = 'error';
			}
		}

		$this->getAjaxResponse()->setSuccess($response);
	}

	public function executeAssignToManager()
	{
		$this->requireManager();

        $response        = [];
        $offerData       = $this->request->getInputJson();
        $offerData['id'] = $offerData['id'] ?? '';

        if ( ! empty( $offerData['id'] ) )
        {
            $offer = Model::loadOffer( (int)$offerData['id'] );

            $offer->setCreatorId( $this->getManager()->getId() );
            $response['result'] = 'assigned';

            if( ! $offer->save() )
            {
                $response['result'] = 'error';
            }

            $response['creator_id'] = (int) $offer->getCreatorId();
        }

        $this->getAjaxResponse()->setSuccess($response);
	}
}