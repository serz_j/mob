<?php

namespace Mobiteasy;
use Connect\Database\QueryCondition;


class Offers_Controller_Api_Deleted extends Controller_Api
{
    public function executeIndex()
    {
        $this->getAjaxResponse()->setFail();
    }

    public function executeDeleted()
    {
        $this->requireManager();

        $response = [];


        $Condition = new QueryCondition();
        $Condition->where = 'status = :status';
        $Condition->bind = [
            ':status' => Model_Record_Offer::STATUS_DELETED,
        ];

        $findBy['status'] = $Condition;

        $offers   = Model::getOffer()->findBy($findBy, 'ORDER BY id ASC');

        foreach ($offers as $k => $offer) {
            $settings    = $offer->getSettings();
            $affiliates  = [];
            $countries   = [];
            $device_type = '';
            $oss         = [];
            $excludes    = [];
            $exclude     = [];
            $carriers    = [];

            if ( isset($settings['rules']) )
            {
                foreach ($settings['rules'] as $rules)
                {
                    $affiliate  = $rules['affiliate'] ?? [];
                    $country    = $rules['country'] ?? [];
                    $os         = $rules['os'] ?? [];
                    $carrier    = $rules['carrier'] ?? [];

                    if( isset($rules['include']) && $rules['include'] == 'exclude' )
                    {
                        $exclude = $rules['affiliate'] ?? [];
                    }

                    $affiliates = array_merge( $affiliate, $affiliates );
                    $countries  = array_merge( $country, $countries );
                    $oss        = array_merge( $os, $oss );
                    $excludes   = array_merge( $exclude, $excludes );
                    $carriers   = array_merge( $carrier, $carriers );

                    $device_type = $rules['device_type'] ?? '';

                }
            }

            $offerData                 = $offer->_getData();
            $offerData['affiliates']   = array_values( array_unique( $affiliates ) );
            $offerData['countries']    = $countries;
            $offerData['device_type']  = $device_type;
            $offerData['carrier']      = $carriers;
            $offerData['excludes'] 	   = array_values( array_unique( $excludes ) );
            $offerData['oss']          = $oss;
            $offerData["type"]         = Model::loadTrafficType($offer->getTrafficTypeId())->getName();
            $offerData['partner_name'] = Model::loadPartner($offer->getPartnerId())->getName();
            $offerData['updated_by']   = Model::loadManager($offer->getManagerId())->getLogin();
            $offerData['created_by']   = empty( $offer->getCreatorId() ) ? 'NONE' : Model::loadManager( $offer->getCreatorId())->getLogin();
            $response['offers'][]      = $offerData;
        }

        $this->getAjaxResponse()->setSuccess($response);
    }

    public function executeTierCountry()
    {
        $this->requireManager();
        $response = [];
        foreach (Model::getDictionary()->getCountry()->getItems() as $Country) {
            if (in_array($Country->getParam('iso2'), Offers_Offers::TIER_1)) {
                $response['tier1'][] = $Country->getId();
            } elseif (in_array($Country->getParam('iso2'), Offers_Offers::TIER_2)) {
                $response['tier2'][] = $Country->getId();
            } elseif(!in_array($Country->getCode(), Model_Record_Dictionary::EXCLUDE_COUNTRY_LIST)) {
                $response['tier3'][] = $Country->getId();
            }
        }
        $this->getAjaxResponse()->setSuccess($response);
    }

    public function executeDirectUrl()
    {
        $this->requireManager();
        $params = $this->request->getInputJson();
        $response['name'] = '';
        if (isset($params['traffic_type_id'])) {
            $response['name'] = Model::getSettings()->getTrafficTypeUrl(Model::getDictionary()->getTrafficType()->getCodeByNr($params['traffic_type_id']));
        }
        $this->getAjaxResponse()->setSuccess($response);
    }
}